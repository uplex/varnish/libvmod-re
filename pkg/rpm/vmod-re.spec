# -D MUST pass in _version and _release, and SHOULD pass in dist.

Summary: Regular Expression module for Varnish Cache
Name: vmod-re
Version: %{_version}
Release: %{_release}%{?dist}
License: BSD
Group: System Environment/Daemons
URL: https://code.uplex.de/uplex-varnish/libvmod-re
Source0: lib%{name}-%{version}.tar.gz

# varnish from varnish60 at packagecloud
# The Requires specifies ABI >= 8.0 (Varnish 6.1, libvarnishapi.so.2)
Requires: varnishd(vrt)%{?_isa} >= 8

BuildRequires: varnish-devel >= 6.1.0
BuildRequires: pkgconfig
BuildRequires: make
BuildRequires: gcc
BuildRequires: python-docutils >= 0.6

# git builds
#BuildRequires: automake
#BuildRequires: autoconf
#BuildRequires: autoconf-archive
#BuildRequires: libtool
#BuildRequires: python-docutils >= 0.6

Provides: vmod-re, vmod-re-debuginfo

%description
Varnish Module (VMOD) for matching strings against regular
expressions, and for extracting captured substrings after matches.

%prep
%setup -q -n lib%{name}-%{version}

%build

# if this were a git build
# ./autogen.sh

%configure

make -j

%check

make check -j

%install

make install DESTDIR=%{buildroot}

# Only use the version-specific docdir created by %doc below
rm -rf %{buildroot}%{_docdir}

# None of these for fedora/epel
find %{buildroot}/%{_libdir}/ -name '*.la' -exec rm -f {} ';'
find %{buildroot}/%{_libdir}/ -name '*.a' -exec rm -f {} ';'

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/varnish*/vmods/
%{_mandir}/man3/*.3*
%doc README.rst LICENSE

%post
/sbin/ldconfig

%changelog
