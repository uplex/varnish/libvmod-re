..
	Copyright 2013-2023 UPLEX Nils Goroll Systemoptimierung
	SPDX-License-Identifier: BSD-2-Clause
	See LICENSE file for full text of license

===================
About this document
===================

.. keep this section at the top!

This document contains a log of noteworthy changes to vmod_re from new
to old.

===============
vmod_re Changes
===============

NEXT
----

* The ``xregex.foreach()`` and ``xregex.foreach_body()`` methods have
  been added to call a VCL subroutine on all matches, which can then
  use ``xregex.backref()`` to access them. Example::

	sub myregex_collect {
		set resp.http.all += myregex.backref(0);
	}

	sub vcl_synth {
		unset resp.http.all;
		myregex.foreach(req.http.input, myregex_collect);
	}

  ``xregex.foreach_body()`` is basically identical, but works on
  bodies.

* With the optional ``asfilter`` argument to ``re.regex()``, a varnish
  filter is created, which can be used to run regular expression
  substitions on bodies, similar to a flexible mix between
  ``regsub()`` and ``regsuball()`` on other strings in VCL.

  In this setup,the ``xregex.substitute_match()`` and
  ``xregex.substitute_all()`` methods can be used to define
  replacements for matches on the body.

* The ``.match_body()`` method has been added to match a regular
  expression as defined by a ``re.regex()`` object against one of the
  following objects:

  * ``req_body`` for ``req.body`` on the client side

  * ``bereq_body`` for ``bereq.body`` on the backend side

  * ``resp_body`` for ``resp.body`` in ``vcl_deliver{}``

  To use this method, the regular expression object needs to be
  constructed with the ``forbody`` flag set in the ``re.regex()``
  constructor.

  The implementation uses PCRE2 multi segment matching to minimize
  memory requirements while still supporting matches across object
  storage segments.

  As with the ``.match()`` method, back-references to the full match
  and sub-expressions are available through
  ``.backref(0)`` .. ``.backref(``\ *n*\ ``)`` after a successful
  ``.match_body()``.

* various refactoring

* adjust to changes in varnish-cache

v2.10.0
-------

suitable for varnish-cache 6.6

corresponds to master commit 96250e04d1d826a4fcf5eb3372e98c4777b6d768

Base of the changelog
