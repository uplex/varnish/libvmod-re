varnishtest ".match_body(resp.body)"

server s1 {
	rxreq
	txresp

	rxreq
	txresp -body "wontmatch"

	# simple case - all in one, C-l
	rxreq
	txresp -body "saldkhaskdhsaksa=123&sadsadjhsakdh82378e3d&b=43875643543"

	rxreq
	txresp -nolen -hdr "Transfer-encoding: chunked"
	chunked "saldkhaskdhs"
	chunked "aksa="
	chunked "12"
	chunked "3&sadsadjhsakdh82378e3d&b=43"
	chunked "875643543"
	chunkedlen 0
} -start

varnish v1 -vcl+backend {
	import re;
	import std;

	sub vcl_init {
		new pattern = re.regex("(a|b)=([^&]*).*&(a|b)=([^&]*)",
		    forbody=true);
	}

	sub vcl_deliver {
		if (! pattern.match_body(resp_body)) {
			set resp.status = 400;
			return (deliver);
		}
		set resp.http.n1 = pattern.backref(1, "");
		set resp.http.v1 = pattern.backref(2, "");
		set resp.http.n2 = pattern.backref(3, "");
		set resp.http.v2 = pattern.backref(4, "");
	}
} -start

client c1 {
	txreq -url "/1"
	rxresp
	expect resp.status == 400

	txreq -url "/2"
	rxresp
	expect resp.status == 400

	txreq -url "/3"
	rxresp
	expect resp.status == 200
	expect resp.http.n1 == "a"
	expect resp.http.v1 == "123"
	expect resp.http.n2 == "b"
	expect resp.http.v2 == "43875643543"

	txreq -url "/4"
	rxresp
	expect resp.status == 200
	expect resp.http.n1 == "a"
	expect resp.http.v1 == "123"
	expect resp.http.n2 == "b"
	expect resp.http.v2 == "43875643543"
} -run
