INSTALLATION
============

RPMs
~~~~

Binary, debuginfo and source RPMs for VMOD re are available at
packagecloud:

	https://packagecloud.io/uplex/varnish

The packages are built for Enterprise Linux 7 (el7), and hence will
run on compatible distros (such as RHEL7, Fedora, CentOS 7 and Amazon
Linux).

To set up your YUM repository for the RPMs, follow these instructions:

	https://packagecloud.io/uplex/varnish/install#manual-rpm

You will also need these additional repositories:

* EPEL7

  * ``yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm``

* Official Varnish packages from packagecloud (since version 6.0.0)

  * Follow the instructions at: https://packagecloud.io/varnishcache/varnish60/install#manual-rpm

If you have problems or questions concerning the RPMs, post an issue
to one of the source repository web sites, or contact
<varnish-support@uplex.de>.

Building from source
~~~~~~~~~~~~~~~~~~~~

The VMOD is built on a system where an instance of Varnish is
installed, and the auto-tools will attempt to locate the Varnish
instance, and then pull in libraries and other support files from
there.

Requirements
------------

.. _`Varnish-Cache build dependencies`: https://varnish-cache.org/docs/trunk/installation/install_source.html

We recommend to just install the `Varnish-Cache build dependencies`_
for the system on which this vmod is to be built.

The following packages are required at a minimum::

  make
  automake
  autoconf
  libpcre2-dev
  libtool
  pkg-config

In particular, ``libpcre2-dev`` refers to a PCRE2 installation with
headers.

Your system might use slightly different names.

Quick start
-----------

This sequence should be enough in typical setups:

1. ``./autogen.sh``  (for git-installation)
2. ``./configure``
3. ``make``
4. ``make check`` (regression tests)
5. ``make install`` (may require root: sudo make install)

Alternative configs
-------------------

If you have installed Varnish to a non-standard directory, call
``autogen.sh`` and ``configure`` with ``PKG_CONFIG_PATH`` pointing to
the appropriate path. For example, when varnishd configure was called
with ``--prefix=$PREFIX``, use::

  PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig
  export PKG_CONFIG_PATH

For developers
--------------

As with Varnish, you can use these ``configure`` options for stricter
compiling:

* ``--enable-developer-warnings``
* ``--enable-extra-developer-warnings`` (for GCC 4)
* ``--enable-werror``

The VMOD must always build successfully with these options enabled.

Also as with Varnish, you can add ``--enable-debugging-symbols``, so
that the VMOD's symbols are available to debuggers, in core dumps and
so forth.
